FROM php:7.3.6-fpm-alpine3.9 AS base
RUN apk update && \
    apk upgrade
RUN apk add --no-cache \
    libpng-dev \
    freetype-dev \
    libjpeg-turbo-dev \
    libzip-dev \ 
    g++
COPY a.txt /var/www/a.txt
COPY b.txt /var/www/b.txt
COPY a.txt /var/www/c.txt
RUN cp $PHP_INI_DIR/php.ini-development $PHP_INI_DIR/php.ini
